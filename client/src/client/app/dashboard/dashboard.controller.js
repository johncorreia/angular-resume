(function() {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardController', DashboardController)
    .directive('fileread', [
      function() {
        return {
          scope: {
            fileread: '='
          },
          link: function(scope, element, attributes) {
            element.bind('change', function(changeEvent) {
              var reader = new FileReader();
              reader.onload = function(loadEvent) {
                scope.$apply(function() {
                  scope.fileread = loadEvent.target.result;
                });
              };
              reader.readAsDataURL(changeEvent.target.files[0]);
            });
          }
        };
      }
    ]);

  DashboardController.$inject = ['$q', 'dataservice', 'logger'];
  /* @ngInject */
  function DashboardController($q, dataservice, logger) {
    var vm = this;
    vm.loginHide = false;
    vm.dashboardHide = true;

    vm.email = '';
    vm.password = '';
    vm.handleLogin = function() {
      dataservice.login(vm.email, vm.password);
      activate();
      vm.loginHide = true;
      vm.dashboardHide = false;
    };

    vm.person = '';
    vm.file = '';
    vm.submitPerson = function() {
      dataservice.addPerson(vm.person, vm.file).then(function(data) {
        console.log(data);
        getPeople();
        vm.person = '';
        vm.file = '';
      });
    };

    vm.deletePerson = function(personID) {
      dataservice.deletePerson(personID);
      vm.hideMenu = true;
      getPeople();
    };

    vm.submitEdits = function() {
      dataservice.editPerson(vm.editPerson.id, vm.edits);
      vm.hideMenu = true;
      getPeople();
    };

    vm.hideMenu = true;
    vm.showEditMenu = function(person) {
      vm.hideMenu = false;
      vm.editPerson = person;
      vm.edits = '';
    };
    vm.closeMenu = function() {
      vm.hideMenu = true;
    };

    vm.refresh = function() {
      getPeople();
    };

    function activate() {
      var promises = [getPeople()];
      return $q.all(promises).then(function() {
        logger.info('Activated Dashboard View');
      });
    }

    function getPeople() {
      return dataservice.getPeople().then(function(data) {
        vm.people = data;
        return vm.people;
      });
    }
  }
})();
