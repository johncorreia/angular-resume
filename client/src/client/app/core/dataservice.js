(function() {
  'use strict';
  angular.module('app.core').factory('dataservice', dataservice);

  dataservice.$inject = ['firebase', '$firebaseObject', 'moment'];

  /* @ngInject */
  function dataservice(firebase, $firebaseObject, moment) {
    var config = {
      apiKey: 'AIzaSyCXHoJqYcn5r1e-uOYlOOxIv-U3ZAXyr64',
      authDomain: 'correia-lab-share.firebaseapp.com',
      databaseURL: 'https://correia-lab-share.firebaseio.com',
      projectId: 'correia-lab-share',
      storageBucket: 'correia-lab-share.appspot.com',
      messagingSenderId: '206274003990'
    };
    firebase.initializeApp(config);
    var storage = firebase.storage().ref('/users');
    var rootRef = firebase
      .database()
      .ref()
      .child('angularJS');
    var ref = rootRef.child('people');
    var firebaseObj = $firebaseObject(rootRef);

    var service = {
      getPeople: getPeople,
      addPerson: addPerson,
      deletePerson: deletePerson,
      editPerson: editPerson,
      login: login
    };

    return service;

    function register(email, password) {
      console.log('register user');
    }

    function login(email, password) {
      firebase.auth().signInWithEmailAndPassword(email, password);
    }

    function getPeople() {
      return firebaseObj
        .$loaded()
        .then(success)
        .catch(fail);

      function success(response) {
        var people = void 0;
        var values = void 0;
        var peopleMap = [];
        people = Object.keys(response.people);
        values = Object.values(response.people);
        return (peopleMap = people.map(function(el, i) {
          return Object.values(response.people)[i];
        }));
      }

      function fail(e) {
        return { failure: 'Failed to get people from firebase' };
      }
    }

    function addPerson(person, cv) {
      var newPerson = ref.push();
      var metadata = {
        contentType: cv
          .split(',')[0]
          .split(';')[0]
          .split(':')[1]
      };
      var base64result = cv.split(',')[1];
      return new Promise(function(resolve, reject) {
        storage
          .child(newPerson.key + '/uploads/' + person)
          .putString(base64result, 'base64', metadata)
          .then(function(data) {
            storage
              .child(newPerson.key + '/uploads/' + person)
              .getDownloadURL()
              .then(function(url) {
                newPerson.set({
                  person: person,
                  id: newPerson.key,
                  date: moment().format('MM/DD/YY'),
                  cv: url
                });
                resolve({ success: 'Added person' });
              })
              .catch(function(err) {
                return console.log(err);
              });
          })
          .catch(function(err) {
            reject(err);
          });
      });
    }

    function deletePerson(personID) {
      console.log('delete this', personID);
      ref.child(personID).remove();
    }

    function editPerson(personID, data) {
      ref.child(personID).update({
        person: data,
        lastUpdated: moment().format('MM/DD/YY HH:mm')
      });
    }
  }
})();
