# AngularJS project

## Usage
To start, cd into `/client` and run `gulp serve-dev`

Example account is:
Email: `test@test.com`
Password: `test123`

You can add a person and a CV, click on a person to edit them, and delete a person as well.

##TODO
Add registration for users
Configure delete to also delete files associated with the user being deleted